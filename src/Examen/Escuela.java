package Examen;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 *  
 */
public class Escuela {
       
    private int numDocen;
    private String nombre;
    private String domici;
    private int nivel;
    private float PagoH;
    private float HorasIm;
  
    
    public Escuela(){
    this.numDocen=0;
    this.nombre="";
    this.nivel=0;
    this.PagoH=0.0f;
    this.HorasIm=0.0f;
    
    
    }
    //Constructor por argumentos
    public Escuela(int numDocen, String nombre, int nivel, float PagoH, float HorasIm){
    this.numDocen=numDocen;
    this.nombre=nombre;
    this.nivel=nivel;
    this.PagoH=PagoH;
    this.HorasIm=HorasIm;
   
    }
    
    //Copia "OTRO"
    public Escuela(Escuela otro){
    this.numDocen=otro.numDocen;
    this.nombre=otro.nombre;
    this.nivel=otro.nivel;
    this.PagoH=otro.PagoH;
    this.HorasIm=otro.HorasIm;

    }
    

    //Metodos Set y Get

    public int getNumDocen() {
        return numDocen;
    }

    public void setNumDocen(int numDocen) {
        this.numDocen = numDocen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomici() {
        return domici;
    }

    public void setDomici(String domici) {
        this.domici = domici;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getPagoH() {
        return PagoH;
    }

    public void setPagoH(float PagoH) {
        this.PagoH = PagoH;
    }

    public float getHorasIm() {
        return HorasIm;
    }

    public void setHorasIm(float HorasIm) {
        this.HorasIm = HorasIm;
    }

    
   
    public float calcularPago(){
    float pago=0.0f;
    float horasT= this.HorasIm*this.PagoH;
    if (this.nivel == 1) {
        pago= horasT*0.3f+horasT;
    }
    if (this.nivel == 2) {
        pago=horasT*0.5f+horasT;
    }
    if (this.nivel == 3) {
         pago=horasT*1.0f+horasT;
    }
    return pago;
    }
    
    public float calcularImpuesto(){
    float impuesto=0.0f;
    impuesto=(this.calcularPago()*0.16f);
    return impuesto;
    }
    
    public float calcularTotal(){
    float total=0.0f;
    total=this.calcularPago()-this.calcularImpuesto();
    return total;
    }
    
}



