package Examen;


import Examen.Escuela;

public class escuela2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Escuela escuela2 = new Escuela();
        escuela2.setNumDocen(100);
        escuela2.setNombre("josé lopez Acosta");
        escuela2.setDomici("av del sol 33");
        escuela2.setNivel(1);
        escuela2.setPagoH(100.0f);
        escuela2.setHorasIm(20.0f);
         
        System.out.println("Num. Recibo: "+ escuela2.getNumDocen());
        System.out.println("Nombre: "+ escuela2.getNombre());
        System.out.println("Nivel: " + escuela2.getNivel()+(" licenciatura/ingenieria"));
        System.out.println("horas Base: "+ escuela2.getPagoH());
        System.out.println("horas Impartidas: "+ escuela2.getHorasIm());
        System.out.println("Calculo de pago total:"+ escuela2.calcularPago());
        System.out.println("calculo de pago con impuesto:"+ escuela2.calcularImpuesto());
        System.out.println("calculo de pago total:"+ escuela2.calcularTotal());
        
    }
    
}
